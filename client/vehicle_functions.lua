local PlayerData              	= {}

local truck	                    = 0
local trailer                   = 0
local deliveryblip

-- Clears the area, so you can spawn and interact with the area around where the truck spawns
function ClearAreaForTruck()
    ClearAreaOfVehicles(Config.TruckProps.SpawnPos.x, Config.TruckProps.SpawnPos.y, Config.TruckProps.SpawnPos.z, 50.0, false, false, false, false, false)
    SetEntityAsNoLongerNeeded(trailer)
    DeleteVehicle(trailer)
    SetEntityAsNoLongerNeeded(truck)
    DeleteVehicle(truck)
    RemoveBlip(deliveryblip)
end

-- Spawns Truck at target location
function SpawnTruck()
    local vehiclehash = GetHashKey(Config.TruckHash)
    RequestModel(vehiclehash)
    while not HasModelLoaded(vehiclehash) do
        RequestModel(vehiclehash)
        Citizen.Wait(0)
    end
    truck = CreateVehicle(vehiclehash, Config.TruckProps.SpawnPos.x, Config.TruckProps.SpawnPos.y, Config.TruckProps.SpawnPos.z, 0.0, true, false)
    SetEntityAsMissionEntity(truck, true, true)
end

-- Spawns Trailer at target location
function SpawnTrailer()
    local trailerhash = GetHashKey(Config.Trailer)
    RequestModel(trailerhash)
    while not HasModelLoaded(trailerhash) do
        RequestModel(trailerhash)
        Citizen.Wait(0)
    end
    trailer = CreateVehicle(trailerhash, Config.TrailerProps.SpawnPos.x, Config.TrailerProps.SpawnPos.y, Config.TrailerProps.SpawnPos.z, 0.0, true, false)
    SetEntityAsMissionEntity(trailer, true, true)
end