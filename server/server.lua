ESX = nil
local dataCache;

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)


--- Spawn a gas truck to do a delivery
ESX.RegisterServerCallback("lucid_gas:getTruck",function(source,cb,id,business)
	local xPlayer = ESX.GetPlayerFromId(source)
	local job_title = xPlayer.job.grade
	local job_name = xPlayer.job.name

	if(CONFIG.debug_mode) then
		return
	else

		-- Check if the person is a manager who can take out a gas truck.
		if job_title == Config.JobTitle and job_name == Config.JobName then

			-- Spawn the gas truck and take away the deposit

		else
			cb(false)
		end
	end
end)

--- onEnteringVehicle
	-- ** IS MANAGER **
		-- The person is assigned to the vehicle for both the deposit and the fuel
		-- Make a blimp to a gas location
		-- The maximum number of gas deliveries is decremented by 1 (max 4 deliveries in 4 days by default)
	-- ** IS NOT ASSIGNED **
		-- The player can drive to another store, they get a percentage of the deposit and the fuel
		-- The player can drive to a blackmarket site, they get a percentage of the deposit and money for the fuel

--- When arrive at gas location
	-- Interact with spot and create waiting timer
	-- Set a meta property to the vehicle upon completion
	-- Set a blimp back to the store



--- When stolen gas truck arrives at blackmarket site
	-- Interact with spot and create waiting timer
	-- Checks if meta includes gas
	-- Sells for percentage of deposit and money for the fuel
    -- vehicle gets deleted

--- When arrives back at the gas station
   	-- The vehicle must be emptied before being deposited
   	-- The truck must have meta
    -- Interact with the parking space to load the gas

--- When a person buys gas
	-- Percentage of the gas goes to the business owner
	-- Money is taken from the civilian


--- DEBUG MODE
	-- Catch how often people visit gas stations
    -- Catch how much gas would be at a default
    -- Catch how much time it takes to make deliveries
    -- Catch how often vehicles intercept other vehicle during delivery (how often they're seen)




