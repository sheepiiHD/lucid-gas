Config = {}

-- Global Configurations
Config.DEBUG_MODE = true
Config.JobName = "24/7"
Config.JobTitle = "Manager"
Config.MaxDeliveriesPerPeriod = 4
Config.PeriodLengthInDays = 4
Config.OwnerKeepPercentage = 1.00
Config.DefaultGasPrice = 2.00

-- Truck Configurations
Config.TruckDeposit = 50000
Config.TruckProps = {
    Hash = 1234123412341234,
    SpawnPos   = {x = 191.73, y = 2799.62, z = 45.87},
    Type  = -1,
}
Config.TrailerProps = {
    Hash = 1234123412341241,
    SpawnPos   = {x = 200.87, y = 2789.53, z = 45.66},
    Type  = -1,
}

Config.GasDropOffLocations = {
	[1] = {name = "Gas Pickup - Vinewood",			x = -1220.50, y = 666.95,   z = 143.10},
	[2] = {name = "Gas Pickup - Vinewood",			x = -1338.97, y = 606.31,   z = 133.37},
	[3] = {name = "Gas Pickup - Rockford",			x = -1051.85, y = 431.66,   z = 76.06 },
	[4] = {name = "Gas Pickup - Rockford",			x = -904.04,  y = 191.49,   z = 68.44 },
	[5] = {name = "Gas Pickup - Rockford",			x = -21.58,   y = -23.70,   z = 72.24 },
	[6] = {name = "Gas Pickup - Hawick",			x = -904.04,  y = 191.49,   z = 68.44 },
	[7] = {name = "Gas Pickup - Alta",				x = 225.39,   y = -283.63,  z = 28.25 },
	[8] = {name = "Gas Pickup - Pillbox",			x = 5.62,     y = -707.72,  z = 44.97 },
	[9] = {name = "Gas Pickup - Mission Row",		x = 284.50,   y = -938.50,  z = 28.35},
	[10] ={name = "Gas Pickup - Rancho",			x = 411.59,   y = -1487.54, z = 29.14},
	[11] ={name = "Gas Pickup - Davis",				x = 85.19,    y = -1958.18, z = 20.12},
	[12] ={name = "Gas Pickup - Chamberlain Hills",	x = -213.00,  y =-1617.35,  z =37.35},
	[13] ={name = "Gas Pickup - La Puerta",			x = -1015.65, y =-1515.05,  z = 5.51},
}

-- Stolen Configurations
Config.StolenTruckPricePercentage = 0.10

