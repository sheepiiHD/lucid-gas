# Gas Delivery and Management
<h4 align="center">
	<a href="#download--installation">Download & Installation</a>
	|
	<a href="#adding-more-logs">Additions</a>
</h4>
<h4 align="center">
	<a href="https://badgen.net/gitlab/release/sheepiiHD/lucid-gas/" title=""><img alt="Licence" src="https://badgen.net/gitlab/release/sheepiiHD/lucid-gas"></a>
	<a href="LICENSE" title=""><img alt="Licence" src="https://badgen.net/gitlab/license/sheepiiHD/lucid-gas"></a>
	<a href="https://discord.gg/lucidcity" title=""><img alt="Discord Status" src="https://discordapp.com/api/guilds/776724970579165186/widget.png"></a>
</h4>
<h4 align="center">
Allows gas stations to be managed, restocked, and have their stock stolen or robbed
</h5>

### Requirements
- FiveM FXServer
- ESX

### v1.0.0
- Configuration added. 

