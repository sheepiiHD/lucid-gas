author 'Sheepii'
description 'Suggestion from revise #9021'
version '1.0.0'

-- Config
server_script 'server_config.lua'
client_script 'client_config.lua'

-- Server Scripts
server_script 'server/server.lua'

--Client Scripts
client_script 'client/client.lua'

game 'gta5'
fx_version 'bodacious'
